# Vintage story modding solution template
This template is the result of way too much work wrangling with the .NET tooling, but the final template is going to help out all users of Visual Studio (which is at least myself) with being more productive while modding, upholding common .NET programming standards and generally standardizing the modding landscape.

## So what does it do?
This template makes use of the .NET CLI to make it easy to create a new solution, with a complete modding environment fully configured and ready:
- Targets .NET Standard 2.0 and can easily add other target frameworks (although the ModLauncher project in the project targets the full .NET framework, which is necessary until VintageStory actually gets updated)
- Can be used through both CLI and visual studio project UI
- Press 'start' and the game starts up automatically and loads your mod, with the full capabilities of the Visual Studio debugger
- There's a PostBuild Task that will do IL Weaving, which means all dependencies are weaved into one dll. This means you can use any dependency and not worry about other mods using the same dependency or another version, because your mod will specifically load the dependency you specified. It can be compared to 'static linking'.
- There's a dotnet CLI template, which means you can easily create new mods that come prepackaged and ready to go. No setup required, besides just installing the nuget package with a single command. This does of course assume you have Visual Studio installed, with the .NET Core tooling selected as part of installation.
- The template takes an input of your install directory, so it is technically platform-independent, and it has options of automatically including the game references (so you don't have to do it manually)
- The template can directly import NuGet packages, also from custom sources, and currently has references to Capsup's Foundation project and CommandSystem project hosted on Gitlab. 
- The template has a ReleaseAndPack Configuration that will automatically pack your mod into a zip which you can directly upload to the forums and the ModDB!
- The mod comes with a nuget.config with a custom NuGet repo already setup so we can all start sharing more code. Ask Capsup#8250 about getting access to this repositroy.
- Works well together with the custom built Gitlab template, used in this very repository, which makes it easy to create new Gitlab releases automatically including automatic changelog generation, automatic versioning based on git tags, nuget package generation and more!

## How do I use it? (quick-start)
1. Install Visual Studio 2019 and make sure you select both the '.NET Core cross-platform development' and '.NET Desktop development' workloads as part of installation (if you already have VS2019 installed and you're unsure if you did, see [here](#how-do-i-check-that-i-already-have-the-correct-visual-studio-installation-workloads))
2. Install [Git](https://git-scm.com/downloads) (basically all default values are fine)
3. (Optional) Sign up for a Gitlab account and use it as your version control to make it easier to generate automatic releases and all the cool stuff Gitlab provides
4. Download the latest NuGet package on the [releases page](https://gitlab.com/vsmods-public/foundation/vsmodtemplate/-/releases)
5. Open a command prompt/git bash/powershell window in the downloaded file folder (Shift + right click on windows > Git bash here)
6. Install the template with `dotnet new -i ./Capsup.VSModTemplate.X.Y.Z.nupkg` (or use tab to auto-fill)
7. The template is now installed and should be available as a Visual Studio project template with C# as language, under the Games category, as well as the dotnet CLI.
8. Define the environment variables for your OS. ([Guide here.](#how-do-i-define-the-values-for-the-environment-variables-on-my-os))
9. Make sure you have the 'Place solution and project in the same directory' checked if you use the Visual Studio UI!

### Visual Studio UI
1. Open Visual Studio 2019
2. In the Solution prompt, press 'Create a new project':
![Solution prompt selection](/uploads/8b2cfad7cfe111c0bb366fc84b28335a/image.png)
3. Select the Vintage Story Mod Solution option, under the Games category:
![Solution template prompt](/uploads/e4673f80074c5c911ee7f319c019be83/unknown.png)
4. Type in the name of your project. Make sure you have the 'Place solution and project in the same directory' checked!
![Solution creation prompt](/uploads/ec554ff4dd464da0b47bc2dbd2897c86/image.png)
5. Fill out the options. You can hover the blue icon for descriptions. **You must generate these two environment variables for your specific OS. ([guide here](#how-do-i-define-the-values-for-the-environment-variables-on-my-os)) or input a path manually. Make sure it is an absolute path!**
![Template creation prompt](/uploads/2c90863ade321e33d4e4e45017f4ee73/image.png)
6. If this windows shows, make sure you press reload:
![image](/uploads/7fbb448d4969412699542b1f049974db/image.png)

### Dotnet CLI
1. Use `dotnet new vsmodsln --help` for help
2. Open a new command prompt/git bash/powershell window where you want the folder for the solution
3. Instantiate the template into the MyAwesomeMod folder: `dotnet new vsmodsln -o MyAwesomeMod`
4. The template defaults to using two non-existing environment variables for the path to your Vintage Story installation and data directories. **You must generate these two environment variables for your specific OS. ([guide here](#how-do-i-define-the-values-for-the-environment-variables-on-my-os)) or input a path manually. Make sure it is an absolute path!**

## Contributions and thanks
- Credits to P3t3rix for the original idea of creating this template
- Credits to Raccoon for helping me quickly test it out
- Credits to SpearAndFang for great feedback

Want more awesome Vintage Story content? Help me spend more time developing mods than enterprise software, by becoming a [patron](https://www.patreon.com/capsup) or donating straight to [paypal](https://paypal.me/capsup1712)!

## FAQ
### How do I check that I already have the correct Visual Studio installation workloads?
##### Windows
1. Press Windows key + R
2. Type `appwiz.cpl` and press enter
4. Find Visual Studio 2019 Community
5. Right click and press 'Change'
![Screenshot 1](/uploads/b0b713221db055d9a96d6ab1d11c22a7/unknown.png)
6. Make sure that you have the '.NET Core cross-platform development' workload (specifically we need .NET Core 2.0 at least)
![.NET core dev workload](/uploads/ab7623f2aaece11308bd23f4134b1d55/image.png)
7. Make sure that you have the '.NET Desktop development' workload (specifically we need .NET Framework 4.5.2 SDK)
![Desktop dev workload](/uploads/5d4681f166c20c3dca8f7f5255c275c5/image.png)
8. If either was missing, make sure the marks are checked and press 'Modify' in the bottom right to finish installation

### How do I define the values for the environment variables on my OS?
##### Windows
1. Press Windows key + R
2. Type `sysdm.cpl` and press enter
3. Press the 'Advanced' tab
![System properties and environment variables](/uploads/693103f17aa663e5676bbb00509dedbd/image.png)
4. Press 'Environment Variables'
5. Make two new **User** variables containing the paths to your installation directories, one called `VINTAGE_STORY` with the full path to your game installation directory. And another called `VINTAGE_STORY_DATA` with the full path to your Vintage Story Data folder, like so:
![Environment Variables](/uploads/6185d98640ada79a719cd4361e8d6230/image.png)
6. Press OK
7. Restart all command prompts/git bash/powershell windows or Visual Studio instances where needed.

### How do I update / uninstall the template?
To update the template, all you need to do is uninstall the old one and install the new one. To uninstall:
1. Open a command prompts/git bash/powershell
2. Use the command `dotnet new -u Capsup.VSModTemplate`
3. If updating, refer to the [quick start](#how-do-i-use-it-quick-start)
