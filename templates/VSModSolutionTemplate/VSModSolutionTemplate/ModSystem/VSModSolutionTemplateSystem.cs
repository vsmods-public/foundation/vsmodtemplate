namespace VSModSolutionTemplate.ModSystem
{
    using Vintagestory.API.Common;
#if (mod_side_server || mod_side_universal)
    using Vintagestory.API.Server;
#endif
#if (mod_side_client || mod_side_universal)
    using Vintagestory.API.Client;
#endif

    public class VSModSolutionTemplateSystem : ModSystem
    {
        public override bool ShouldLoad(EnumAppSide forSide)
        {
#if mod_side_universal
            return true;
#elif mod_side_server
            return forSide == EnumAppSide.Server;
#elif mod_side_client
            return forSide == EnumAppSide.Client;
#endif
        }
#if (mod_side_server || mod_side_universal)

        public override void StartServerSide(ICoreServerAPI api)
        {

        }
#endif
#if (mod_side_client || mod_side_universal)

        public override void StartClientSide(ICoreClientAPI api)
        {

        }
#endif
    }
}
