namespace VSModSolutionTemplate.ModConfiguration
{
#if enable_commandsystem
    using System;
#endif
#if enable_foundation
    using Foundation.ModConfig;
#endif
#if enable_commandsystem
    using CommandSystem2.Entities.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    using Newtonsoft.Json;
#endif
#if enable_foundation

#if enable_commandsystem
    public class ModConfig : ModConfigBase, ICommandSystemConfig
#else
    public class ModConfig : ModConfigBase
#endif
#elif
    public class ModConfig : ICommandSystemConfig
#else
    public class ModConfig
#endif
    {
#if enable_foundation
        //Foundation config:
        public override string ModCode => ModConstants.DomainName;
#endif
#if enable_commandsystem

        //Command System config:
        [JsonIgnore]
        public IServiceProvider Services { get; set; } = new ServiceCollection().BuildServiceProvider(true);
        [JsonIgnore]
        public bool IgnoreExtraArguments => false;
#endif
    }
}
