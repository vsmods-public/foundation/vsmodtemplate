## 0.5.16 (2021-07-26)

### Bug fix (1 change)

- [Correct namespace when using foundation and commandsystem](vsmods-public/foundation/vsmodtemplate@25aab7a6603291341bad9b76786407a501f091b7)

### Feature change (1 change)

- [Update TargetFramework of VSModLauncher to 461 to support v1.15](vsmods-public/foundation/vsmodtemplate@e77f8f174ab983334b7aa86ec758586081ac09be)

## 0.5.15 (2021-07-18)

### New feature (1 change)

- [Add Message to ReleaseAndZip to make it slightly more verbose](vsmods-public/foundation/vsmodtemplate@f64806b22c387d0314e3f48b5dfef60f0d89b095)

### Bug fix (2 changes)

- [Update modinfo.json to use the dependencies attribute](vsmods-public/foundation/vsmodtemplate@714bcf79a9492fc6e5da336b684d9b7a54cba356)
- [Make ModSystem use block body for ShouldLoad instead](vsmods-public/foundation/vsmodtemplate@1ca9dbe85ec838303c3f2883b9eee8e2894d6a88)

## 0.5.14 (2021-06-28)

### New feature (4 changes)

- [Add 'Include git & Gitlab'-checkbox that enables the pipeline for a new mod...](vsmods-public/foundation/vsmodtemplate@fade514caa5116a49f28f612e4b4422836001885)
- [The ReleaseAndZip configuration will now name the output zip with the version...](vsmods-public/foundation/vsmodtemplate@d4c94a2fc693fea3edd19a41695d28efc61ed1c1)
- [Pipeline now supports building mods with game dependencies](vsmods-public/foundation/vsmodtemplate@3d700bad75001c2ef2eb29055e7793da706a73cc)

### Bug fix (2 changes)

- [Add trailing / to output path so building zip works in pipeline](vsmods-public/foundation/vsmodtemplate@0abdcf82c5a1fb1c11c0b0b1f2cb2898eca91f17)
- [Fix whitespaces and rename solution file so Visual Studio does not ask for reload upon generation](vsmods-public/foundation/vsmodtemplate@0e56e36922a7669a45bea44c2b990776758c4483)

### Feature change (1 change)

- [Make the template pipeline update the mod pipeline so it always points to a specific tag version](vsmods-public/foundation/vsmodtemplate@6b441c6633e5be667be3998cba674bff2463f30f)

### Other (1 change)

- [Set default version in modinfo to 0.1.0 to satisfy MinVer best practices](vsmods-public/foundation/vsmodtemplate@0090255c9fbd0920f94eef3a30eb6b6d1f4c5f8b)

## 0.4.15 (2021-06-11)

### New feature (1 change)

- [Make build jobs import the game dependencies for compiling](vsmods-public/foundation/vsmodtemplate@775a6b0dc8ce141b2dcee078fc975cf733b54927)

### Bug fix (1 change)

- [Remove unneeded blank lines](vsmods-public/foundation/vsmodtemplate@491430b89b36d861329cf6cf220eaa0721c80bdf)

### Feature change (2 changes)

- [Use Gitlab uploads instead of artifacts for release assets](vsmods-public/foundation/vsmodtemplate@a7c76533baaba93c5042cf83aac7e806943fbd44)
- [Make the editorconfig not suggest changing methods, constructors, operators...](vsmods-public/foundation/vsmodtemplate@e6b52ea83c23208a812fa49c8ffb8564ecad035e)

## 0.4.9 (2021-06-09)

### Bug fix (1 change)

- [Change asset URLs to use tags to make sure they are always valid](vsmods-public/foundation/vsmodtemplate@f8bab05bc024abd5b0aaf38d3b36529d9f2cbe61)

## 0.4.6 (2021-06-09)

### New feature (1 change)

- [Implement option to add release zip to Automatic release](vsmods-public/foundation/vsmodtemplate@88b5d013e6a6c4e157423a51037b4380e9c3be3b)
